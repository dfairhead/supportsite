#!.v/bin/python

import os
import json

from flask import Flask, render_template, send_file, request, jsonify, \
    safe_join,Response

from flask.views import MethodView
from subprocess import check_call
from os.path import splitext, exists, dirname
from os import makedirs

import config
import generate

app = Flask(__name__)

ROOT = os.path.abspath(os.path.dirname(__file__))

#################
# Very basic authorizaion:

_AUTH_REQUEST = Response('Sorry, Permission Denied', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})

def is_authorized():
    auth = request.authorization
    return (auth and ((auth.username, auth.password) in config.ADMINS))

##################
# Some 'basic' bits:

@app.route('/favicon.ico')
def favicon():
    return '', 404

@app.route('/')
def index():
    return send_file(safe_join(ROOT, 'static/index.html'))

@app.route('/admin')
def admin():
    if not is_authorized():
        return _AUTH_REQUEST

    with open(safe_join(ROOT, 'static/index.html')) as f:
        return f.read() + '<script src="/static/admin.js"></script>'

@app.route('/update', methods=['POST'])
def update_cache():
    generate.gencache()
    return send_file(config.ALL_CACHE)

@app.route('/all.json')
def alljson():
    #try:
    #    return send_file(config.ALL_CACHE)
    #except FileNotFoundError:
    generate.gencache()
    return send_file(config.ALL_CACHE)


class DataFile(MethodView):
    def get(self, path):
        fullpath = safe_join(config.DATA_DIR, path)

        if os.path.isfile(fullpath):
            return send_file(fullpath)
        return '', 404

    def put(self, path):
        if not is_authorized():
            return _AUTH_REQUEST

        fullpath = safe_join(config.DATA_DIR, path)

        if not os.path.isfile(fullpath):
            return '', 404

        data = request.get_json()
        return jsonify(generate.updatefile(fullpath, data))

    def delete(self, path):
        if not is_authorized():
            return _AUTH_REQUEST

        fullpath = safe_join(config.DATA_DIR, path)

        if not os.path.isfile(fullpath):
            return '', 404

        generate.deletefile(fullpath)

        return '{}', 200

app.add_url_rule('/<path:path>', view_func=DataFile.as_view('datafile'))

@app.route('/thumbnails/<path:path>')
def thumbnail(path):
    thumbnailpath = safe_join(config.THUMBNAILS_DIR, path)
    if not exists(thumbnailpath):
        orig, _ = splitext(path)
        orig = orig[len(config.DOCS_URL):]
        orig = safe_join(config.DATA_DIR, orig)
        if not exists(orig):
            raise FileNotFoundError(orig)
            return 'NONE'

        #if orig.lower().endswith('.pdf'):
        orig += '[0]'

        makedirs(dirname(thumbnailpath), exist_ok=True)
        check_call(['convert', orig, '-scale', '100x100>', thumbnailpath])

    return send_file(thumbnailpath)

if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
