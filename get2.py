#!/home/daniel/src/support-malu-marine/.v/bin/python3
import sys
import io
import getopt

from pdfminer.pdfinterp import PDFResourceManager, process_pdf
from pdfminer.pdfdevice import TagExtractor
from pdfminer.converter import XMLConverter, HTMLConverter, TextConverter
from pdfminer.layout import LAParams
from pdfminer.utils import set_debug_logging

def convert(filename, outfile=None):

    # input option
    pagenos = set()
    maxpages = 0
    # output option
    outtype = 'text'
    caching = True
    laparams = LAParams()
    rsrcmgr = PDFResourceManager(caching=caching)


    if outfile:
        outfp = io.open(outfile, 'wt', encoding='utf-8', errors='ignore')
        close_outfp = True
    else:
        outfp = sys.stdout
        close_outfp = False

    fp = io.open(filename, 'rb')
    device = TextConverter(rsrcmgr, outfp, laparams=laparams)
    process_pdf(rsrcmgr, device, fp, pagenos, maxpages=maxpages,
                caching=caching, check_extractable=True)
    fp.close()

    device.close()
    if close_outfp:
        outfp.close()

if __name__ == '__main__':
    convert(sys.argv[1])
