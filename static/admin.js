/*
 * admin.js - the admin mixin / additions for supportsite.
 * ---
 *  Mithril stuff.
 */

/****************************
 *
 * Callbacks:
 *
 *****************************/

function editfile(file) {
	return function() {
		state.editfile = file;
	};

	return function() {
		var answer = prompt('Change Displayed name:', file.name).trim();
		if (answer) {
			file.name = answer;
			m.request({method:'PUT',
				url:'/' + file.path,
				data:{'path': file.path, 'newname': answer}
				})
		}
	}
}

function deletefile(file) {
	return function() {
		var really = confirm('Really delete:' + file.name + '\n(' + file.path + ')');
		if (really) {
			m.request({method:'DELETE',
				url:'/' + file.path,
				data: {'path': file.path}
			})
			file.deleted = true;
		}
	}
}

var FileRow = {
	view: function(vnode) {
		var file = vnode.attrs.file;
		return m('tr', {'class': file.deleted ? 'deleted' : ''}, [
				m('td.name', [
					m('button.edit',{onclick: editfile(file)}, 'EDIT'),
					m('a', {href: file.path}, [
						icon(file),
						file.name
						]),
					]),

				m('td.mdate', new Date(file.mdate * 1000).toLocaleDateString()),
				m('td.size', [
					sizer(file.size),
					m('button.delete', {onclick: deletefile(file)}, '✘')
					])
			]);
	}
};

function databind(attrname, object, key, more) {
	more = more || {};
	more['oninput'] = m.withAttr(attrname, function(x){object[key]= x;}),
	more[attrname] = object[key];
	return more;
}

var EditFile = {
	view: function(vnode) {
		var file = vnode.attrs.file;
		return m('#editfile', [
			m('h2', file.path),
			m('#closeedit', {onclick: function(){ state.editfile = false;}}, 'x'),
			m('label[for=editname]', 'Name:'),
			m('input[name=editname]', databind('value', file, 'name')),
			m('label[for=editdesc]', 'Description:'),
			m('textarea[name=editdesc]', databind('value', file, 'desc')),
		]);
	}
}

window.onkeydown = function (evt) {
	if (evt.keyCode == 27) {
		state.editfile = false;
		m.redraw();
	}
}
