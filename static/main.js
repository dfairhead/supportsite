'use strict';

// 'globals', probably should move:

var root = document.getElementById('main'),
	tree = {}; // full tree

// some helper functions:

function sizer(size) {
	/* Human Friendly File Sizes */
	if (size > 1073741824) {
		return (size / 1073741824).toFixed(2).replace(/0+$/,'') + 'Gb'
	} else if (size > 1048576) {
		return (size / 1048576).toFixed(2).replace(/0+$/,'') + 'Mb'
	} else {
		return (size / 1024).toFixed(2).replace(/0+$/,'') + 'Kb'
	}
}

// Components:

var icon_show_self = ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg'];
var icon_show_thumbs = ['jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg', 'pdf', 'ps'];

function icon(file) {
	var exten = file.path.substr(file.path.lastIndexOf('.')+1).toLowerCase() || 'other';
	if (icon_show_self.indexOf(exten) !== -1) {
		if (file.size < 10000) {
            return m('img', {src: file.path});
		}
	}
	if (icon_show_thumbs.indexOf(exten) !== -1) {
        return m('img', {src: '/thumbnails' + file.path + '.png'});
    }
    return m('img', {src: '/static/icons/' + exten + '.svg'});
}


var FileRow = {
	view: function(vnode) {
		var file = vnode.attrs.file;
		return m('tr', [
                m('td.icon', m('a', {href: file.path}, icon(file))),
				m('td.name', m('a', {href: file.path}, [
					file.name
					])),
				m('td.mdate', new Date(file.mdate * 1000).toLocaleDateString()),
				m('td.size', sizer(file.size))
			]);
	}
}

// And the actual App State:

var state = {
	search: '',
	search_reg: new RegExp("","i"),
	update_search: function(value) {
		state.search = value;
		state.search_reg = new RegExp(value, 'i');
	}
}

// Add a spinner for when links are clicked:

document.body.onclick = function(e) {
	if ((e.target.tagName == 'A')&&(e.buttons==0)) {
		document.body.classList.add('loading');
	}
}

// And the main route:

var DirRoute = {
	view: function(vnode) {
		var
			path = vnode.attrs.path,
			dirs = path.split('/'),
			treenode = tree;

		if (!treenode.hasOwnProperty('dirs')) return m('h1','loading...');

		if (document.body.classList.contains('loading')) {
			requestAnimationFrame(function() {
				document.body.classList.remove('loading');
			})
		}

		for(var i=1;i<dirs.length;i++){
			treenode = treenode.dirs.find(function(d){
				return d.name == dirs[i]});
		}
		

		return m('#all', [
			(state.editfile?m(EditFile, {file: state.editfile}):''),
			m('#head', [
				(dirs.length==1 ? '' : m('h1', [
					m('a', {
						href: '/' + dirs.slice(0,-1).join('/'),
						oncreate: m.route.link}, '↰'),
					vnode.attrs.path])),
				m('input#mobilefilter.mobileonly',
					{oninput: m.withAttr('value', state.update_search),
					value: state.search, placeholder: '🔍 Filter by name'})
				]),
			m('table#contents' , [
				m('thead', [
					m('tr', [
						m('td.name', {colspan:2},['Name',
				m('input',
					{oninput: m.withAttr('value', state.update_search),
					value: state.search, placeholder: '🔍 Filter by name'})
                        ]),
						m('td.mdate', 'Modified'),
						m('td.size', 'Size'),
					])
				]),
				m('tbody#folders', treenode.dirs.map(function(dir) {
					return m('tr', [
						m('td', m('a', {href:'/' + dir.path,oncreate:m.route.link}, '📂' + dir.name) )
					]);
					})
				),
				m('tbody#files', treenode.files.filter(function(file){
					return (state.search_reg.test(file.name));
				}).map(function(file){
					return m(FileRow, {file:file, id: file.name})
					})
				)
			])
		]);
	}
}

// Load up the initial data:

m.request({'url':'/all.json'}).then(function(data) {tree = data});

// The search tool is non-Mithril for now:

document.getElementById('fullsearch').onclick = function () {
    var search = prompt('Search for:');
    if (search) {
        document.location = encodeURI('/search?q=' + search);
    }
}

// Initial loading spinning mouse until first render:

document.body.classList.add('loading');

// And set up the routing table

m.route(root, '/', { '/:path...': DirRoute })
