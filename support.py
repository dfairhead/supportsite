import os
import json

from flask import Flask, render_template, send_file

import config
import generate

app = Flask(__name__)


#def frontpage():
#    return 'Front Page'

@app.route('/favicon.ico')
def favicon():
    return '', 404

@app.route('/')
@app.route('/<path:folder>')
def folderlist(folder=''):
    fullpath = os.path.join(config.DATA_DIR, folder)

    if os.path.isfile(fullpath):
        return send_file(fullpath)

    try:
        with open(os.path.join(folder,'.info')) as infofile:
            info=json.read(infofile)
    except OSError:
        info={}

    return render_template('dir.html',
        config=config,
        path=folder,
        info=info.get('.'),
        dirs=generate.dirs(fullpath),
        files=generate.files(fullpath))

if __name__ == '__main__':
    app.run(debug=True)
