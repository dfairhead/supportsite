import ujson
import os
from datetime import datetime
from functools import wraps

from os.path import isdir, isfile, join, dirname, basename, relpath

import config

def in_datadir(func):
    @wraps(func)
    def wrapped(*vargs, **kwargs):
        os.chdir(config.DATA_DIR)
        return func(*vargs, **kwargs)
    return wrapped


def filedetails(path, filename):
    data = {'name':filename}
    metafile = metadatafile(join(path, filename))
    try:
        with open(metafile) as f:
            data.update(ujson.load(f))
    except OSError:
        pass

    path = join(path, filename)
    stats = os.stat(path)
    data.update(path=config.DOCS_URL + path[1:])
    data.update(mdate=stats.st_mtime)
    data.update(size=stats.st_size)

    return data

def metadatafile(filename):
    return join(dirname(filename), '.' + basename(filename) + '.json')

@in_datadir
def deletefile(apath):

    infopath = metadatafile(apath)
    os.rename(apath, join(config.TRASH_DIR, basename(apath)))
    try:
        os.rename(infopath, join(config.TRASH_DIR, basename(infopath)))
    except FileNotFoundError:
        pass

    gencache()

@in_datadir
def updatefile(apath, form):
    path = './' + relpath(apath)
    filename = basename(path)

    infopath = metadatafile(apath)

    try:
        with open(infopath, 'r') as f:
            data = ujson.load(f)
    except OSError:
        data = {}

    if 'newname' in form:
        data.update(name=form['newname'])

    #########
    # update this file metadata

    with open(infopath, 'w') as f:
        ujson.dump(data, f)

    gencache()

    return data


def dirs(path, only_names=False):
    for d in sorted(os.listdir(path)):
        if isdir(join(path, d)) and not d.startswith('.'):
            if only_names:
                yield d
            else:
                yield {'name':d, 'path': d}


def files(path):
    for f in sorted(os.listdir(path)):
        if isfile(join(path, f)) and not f.startswith('.'):
            yield filedetails(path, f)

@in_datadir
def gendict(path):
    try:
        with open(join(path,'.INFO')) as infofile:
            info = ujson.load(infofile)
    except:
        info = {'name': os.path.basename(path), 'path': path}

    info.update(dirs=[gendict(join(path, d)) for d in dirs(path, only_names=True)],
        files=[f for f in files(path)])
    info.update()
    return info

def gencache():
    with open(config.ALL_CACHE, 'w') as f:
        ujson.dump(gendict('.'), f,
            indent=1,
            ensure_ascii=False,
            double_precision=0,
            escape_forward_slashes=False)

if __name__ == '__main__':
    gencache()
