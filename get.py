#!/home/daniel/src/support-malu-marine/.v/bin/python3
import sys
import io

from pdfminer.pdfinterp import PDFResourceManager, process_pdf
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams

def convert(filename, outfp=sys.stdout):

    # input option
    pagenos = set()
    maxpages = 0
    # output option
    caching = True
    rsrcmgr = PDFResourceManager(caching=caching)

    device = TextConverter(rsrcmgr, outfp, laparams=LAParams())

    with open(filename, 'rb') as fp:
        process_pdf(rsrcmgr, device, fp, pagenos, maxpages=maxpages,
                    caching=caching, check_extractable=True)

if __name__ == '__main__':
    if len(sys.argv) == 3:
        with open(sys.argv[2], 'w') as outfile:
            convert(sys.argv[1], outfile)
    elif len(sys.argv) == 2 and sys.argv[1] not in ('-h', '--help'):
        convert(sys.argv[1], sys.stdout)
    else:
        print ('Usage: %s <input.pdf> [<output.txt>]\n'
               '  default output is stdout.' % sys.argv[0])
