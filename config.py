'''
    Support Site Config.
    Usually you should not be editing values in here, but rather in 
    config_local.py
    they override what's in here.
'''
from os.path import abspath, dirname, join

TITLE='Support Site'
ADMINS=[] # [('username','password')...]


ROOT = dirname(abspath(__name__))
DATA_DIR=join(ROOT, 'data')
ALL_CACHE=join(ROOT, 'all.json')
TRASH_DIR=join(ROOT, 'Trash')
THUMBNAILS_DIR=join(ROOT, 'thumbnails')

DOCS_URL='/docs'


try:
    from config_local import *
except ImportError:
    pass
